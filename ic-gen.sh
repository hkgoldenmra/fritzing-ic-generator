#!/bin/bash

IFS=$'\n'
pathname="${1}"

if [ -f "${pathname}" ] && [ `echo "${pathname}" | grep '\.json$'` ]; then
	json=`cat "${pathname}" | jq "."`
	if ! [ "${?}" = "0" ]; then
		exit
	fi

	uuid=`uuidgen`

	length=`echo "${json}" | jq ".connectors | length"`
	ic_name=`echo "${json}" | jq -r ".name"`
	ic_description=`echo "${json}" | jq -r ".description"`
	ic_type=`echo "${json}" | jq -r ".type" | tr "[[:lower:]]" "[[:upper:]]"`
	ic_size=`echo "${json}" | jq -r ".size"`
	bus_length=`echo "${json}" | jq ".buses | length"`

	pathname=`basename "${pathname:0:-5}"`"-${ic_type}-${uuid}"
	part="part.${pathname}.fzp"
	icon="svg.icon.${pathname}.svg"
	breadboard="svg.breadboard.${pathname}.svg"
	schematic="svg.schematic.${pathname}.svg"
	pcb="svg.pcb.${pathname}.svg"

	default_xml='<?xml version="1.0" encoding="UTF-8"?>'
	default_doc='<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">'
	version="0.9.6"
	multiply="1000"
	name_font_size=$((5*$multiply))
	pin_font_size=$((2*$multiply))

	notch_ratio=$(jq -n "4/3*(`echo 2 | jq sqrt`-1)")
	notch_x=$((5*$multiply))
	notch_y=$((5*$multiply))
	notch="M 0,0 C `jq -n $notch_x*$notch_ratio`,0 $(($notch_x)),`jq -n $notch_y-$notch_y*$notch_ratio` $(($notch_x)),$(($notch_y)) C $(($notch_x)),`jq -n $notch_y+$notch_y*$notch_ratio` `jq -n $notch_x*$notch_ratio`,$(($notch_y*2)) 0,$(($notch_y*2)) Z"

	part_fzp="${default_xml}"
	part_fzp+="\n<module fritzingVersion='${version}' moduleId='${uuid}'>"
	part_fzp+="\n	<title>${ic_name}</title>"
	part_fzp+="\n	<description>${ic_description}</description>"
	part_fzp+="\n	<views>"
	part_fzp+="\n		<iconView>"
	part_fzp+="\n			<layers image='icon/${pathname}.svg'>"
	part_fzp+="\n				<layer layerId='icon'/>"
	part_fzp+="\n			</layers>"
	part_fzp+="\n		</iconView>"
	part_fzp+="\n		<breadboardView>"
	part_fzp+="\n			<layers image='breadboard/${pathname}.svg'>"
	part_fzp+="\n				<layer layerId='breadboard'/>"
	part_fzp+="\n			</layers>"
	part_fzp+="\n		</breadboardView>"
	part_fzp+="\n		<schematicView>"
	part_fzp+="\n			<layers image='schematic/${pathname}.svg'>"
	part_fzp+="\n				<layer layerId='schematic'/>"
	part_fzp+="\n			</layers>"
	part_fzp+="\n		</schematicView>"
	part_fzp+="\n		<pcbView>"
	part_fzp+="\n			<layers image='pcb/${pathname}.svg'>"
	part_fzp+="\n				<layer layerId='copper1'/>"
	part_fzp+="\n				<layer layerId='silkscreen'/>"
	if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
	part_fzp+="\n				<layer layerId='copper0'/>"
	fi
	part_fzp+="\n			</layers>"
	part_fzp+="\n		</pcbView>"
	part_fzp+="\n	</views>"
	part_fzp+="\n	<tags>"
	part_fzp+="\n		<tag>A IC</tag>"
	part_fzp+="\n	</tags>"
	part_fzp+="\n	<properties>"
	part_fzp+="\n		<property name='family'>A IC</property>"
	part_fzp+="\n		<property name='a-package'>${ic_type}</property>"
	part_fzp+="\n		<property name='a-pins'>${length}</property>"
	part_fzp+="\n	</properties>"
	part_fzp+="\n	<buses>"
	i="0"
	while [ "${i}" -lt "${bus_length}" ]; do
		bus_name=`echo "${json}" | jq -r ".buses[${i}].name"`
		node_length=`echo "${json}" | jq ".buses[${i}].nodes | length"`
	part_fzp+="\n		<bus id='${bus_name}'>"
		l="0"
		while [ "${l}" -lt "${node_length}" ]; do
			node=`echo "${json}" | jq -r ".buses[${i}].nodes[${l}]"`
			node=$(($node+1))"-"`echo "${json}" | jq -r ".connectors[${node}].name"`
	part_fzp+="\n			<nodeMember connectorId='connector-${node}'/>"
			l=$(($l+1))
		done
	part_fzp+="\n		</bus>"
		i=$(($i+1))
	done
	part_fzp+="\n	</buses>"
	part_fzp+="\n	<connectors>"

	if [ "${ic_type}" = "SIP" ]; then
		icon_pin_size=$((10*$multiply))
		icon_pin_interval=$((10*$multiply))
		icon_ic_width=$((3*$icon_pin_interval))
		icon_ic_height=$((20*$multiply))
		icon_ic_notch_y=$(($icon_ic_height/2-5*$multiply))
		icon_ic_name_x=$((2*$notch_x))
		icon_ic_name_y=$(($name_font_size*3/10+$icon_ic_height/2))
		icon_1st_pin_x=$(($icon_pin_interval/2))
		icon_1st_pin_y=$(($icon_ic_height-$icon_ic_height/8))
		icon_1st_pin_radius=$((1*$multiply))
		icon_width=$(($icon_ic_width))
		icon_height=$(($icon_ic_height+$icon_pin_size))

		breadboard_pin_size=$((10*$multiply))
		breadboard_pin_interval=$((10*$multiply))
		breadboard_ic_width=$(($length*$breadboard_pin_interval))
		breadboard_ic_height=$((20*$multiply))
		breadboard_ic_notch_y=$(($breadboard_ic_height/2-5*$multiply))
		breadboard_ic_name_x=$(($breadboard_ic_width/2))
		breadboard_ic_name_y=$(($name_font_size*3/10+$breadboard_ic_height/2))
		breadboard_1st_pin_x=$(($breadboard_pin_interval/2))
		breadboard_1st_pin_y=$(($breadboard_ic_height-$breadboard_ic_height/8))
		breadboard_1st_pin_radius=$((1*$multiply))
		breadboard_width=$(($breadboard_ic_width))
		breadboard_height=$(($breadboard_ic_height+$breadboard_pin_size))

		schematic_pin_text_offset=$((4*$multiply))
		schematic_pin_size=$((20*$multiply))
		schematic_pin_interval=$((10*$multiply))
		schematic_ic_width=$(($length*$schematic_pin_interval))
		schematic_ic_height=$((20*$multiply))
		schematic_ic_name_x=$(($schematic_ic_width/2))
		schematic_ic_name_y=$(($name_font_size*3/10+$schematic_ic_height/2))
		schematic_1st_pin_x=$(($schematic_pin_interval/2))
		schematic_1st_pin_y=$(($schematic_ic_height-$schematic_ic_height/8))
		schematic_1st_pin_radius=$((1*$multiply))
		schematic_width=$(($schematic_ic_width))
		schematic_height=$(($schematic_ic_height+$schematic_pin_size))

		pcb_pin_text_offset=$((6*$multiply))
		pcb_pin_radius=$((3*$multiply))
		pcb_pin_width=$((15*$multiply/10))
		pcb_pin_size=$((10*$multiply))
		pcb_pin_interval=$((10*$multiply))
		pcb_ic_width=$(($length*$pcb_pin_interval))
		pcb_ic_height=$((10*$ic_size*$multiply))
		pcb_ic_name_x=$(($pcb_ic_width/2))
		pcb_ic_name_y=$(($name_font_size*3/10+$pcb_pin_size/2))
		pcb_1st_pin_x=$(($pcb_pin_interval/2))
		pcb_1st_pin_y=$(($pcb_ic_height-$pcb_pin_size/2))
		pcb_width=$(($pcb_ic_width))
		pcb_height=$(($pcb_ic_height))

		part_connectors_fzp=''
		breadboard_pin_svg=''
		schematic_text_svg=''
		schematic_pin_svg=''
		pcb_text_svg=''
		pcb_pin_svg=''

		i="0"
		while [ "${i}" -lt "${length}" ]; do
			pin_label=`echo "${json}" | jq -r ".connectors[${i}].name"`
			pin_name=$(($i+1))"-${pin_label}"
			pin_description=`echo "${json}" | jq -r ".connectors[${i}].description"`
			echo "Pin $(($i+1))/$(($length)) - ${pin_label}"

			# part middle
			part_connectors_fzp+="\n		<connector id='connector-${pin_name}' name='${pin_label}' type='male'>"
			part_connectors_fzp+="\n			<description>${pin_description}</description>"
			part_connectors_fzp+="\n			<views>"
			part_connectors_fzp+="\n				<breadboardView>"
			part_connectors_fzp+="\n					<p layer='breadboard' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</breadboardView>"
			part_connectors_fzp+="\n				<schematicView>"
			part_connectors_fzp+="\n					<p layer='schematic' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</schematicView>"
			part_connectors_fzp+="\n				<pcbView>"
			part_connectors_fzp+="\n					<p layer='copper1' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
			part_connectors_fzp+="\n					<p layer='copper0' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			fi
			part_connectors_fzp+="\n				</pcbView>"
			part_connectors_fzp+="\n			</views>"
			part_connectors_fzp+="\n		</connector>"

			# schematic text
			schematic_text_svg+="\n				<text x='0' y='$(($i*$schematic_pin_interval))'>${pin_label}</text>"

			# pcb text
			pcb_text_svg+="\n				<text x='$((-$pcb_height+$pcb_pin_interval))' y='$(($i*$pcb_pin_interval+$pcb_pin_text_offset))'>${pin_label}</text>"

			if [ "${i}" -gt "0" ]; then
				# breadboard other pins
				breadboard_pin_svg+="\n			<g transform='translate($(($i*$breadboard_pin_interval)),0)'>"
				breadboard_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
				breadboard_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
				breadboard_pin_svg+="\n			</g>"

				# schematic other pins
				schematic_pin_svg+="\n				<g transform='translate($(($i*$schematic_pin_interval)),0)'>"
				schematic_pin_svg+="\n					<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
				schematic_pin_svg+="\n					<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
				schematic_pin_svg+="\n				</g>"

				# pcb other pins
				pcb_pin_svg+="\n			<g transform='translate($(($i*$pcb_pin_interval)),0)'>"
				pcb_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
				pcb_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
				pcb_pin_svg+="\n			</g>"
			else
				template="${pin_name}"
				# breadboard 1st pin
				breadboard_pin_svg+="\n			<g>"
				breadboard_pin_svg+="\n				<polygon id='connector-${pin_name}-pin' fill='#888888' points='$(($breadboard_1st_pin_x-2500)),$(($breadboard_height-$breadboard_pin_size)) $(($breadboard_1st_pin_x-2500)),$(($breadboard_height-$breadboard_pin_size+1*$multiply)) $(($breadboard_1st_pin_x-500)),$(($breadboard_height-$breadboard_pin_size+2*$multiply)) $(($breadboard_1st_pin_x-500)),$(($breadboard_height)) $(($breadboard_1st_pin_x+500)),$(($breadboard_height)) $(($breadboard_1st_pin_x+500)),$(($breadboard_height-$breadboard_pin_size+2*$multiply)) $(($breadboard_1st_pin_x+2500)),$(($breadboard_height-$breadboard_pin_size+1*$multiply)) $(($breadboard_1st_pin_x+2500)),$(($breadboard_height-$breadboard_pin_size))'/>"
				breadboard_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='5000' cy='$(($breadboard_height))' r='0.001' fill='none'/>"
				breadboard_pin_svg+="\n			</g>"

				# schematic 1st pin
				schematic_pin_svg+="\n				<g>"
				schematic_pin_svg+="\n					<polyline id='connector-${pin_name}-pin' points='$(($schematic_1st_pin_x)),$(($schematic_height-$schematic_pin_size)) $(($schematic_1st_pin_x)),$(($schematic_height))' stroke='#888888'/>"
				schematic_pin_svg+="\n					<circle id='connector-${pin_name}-terminal' cx='$(($schematic_1st_pin_x))' cy='$(($schematic_height))' r='0.001' fill='none'/>"
				schematic_pin_svg+="\n				</g>"

				# pcb 1st pin
				pcb_pin_svg+="\n			<g>"
				pcb_pin_svg+="\n				<rect x='$(($pcb_1st_pin_x-$pcb_pin_radius))' y='$(($pcb_1st_pin_y-$pcb_pin_radius))' width='$(($pcb_pin_radius*2))' height='$(($pcb_pin_radius*2))' fill='none' stroke='#FFCC00' stroke-width='$(($pcb_pin_width))'/>"
				pcb_pin_svg+="\n				<circle id='connector-${pin_name}-pin' cx='$(($pcb_1st_pin_x))' cy='$(($pcb_1st_pin_y))' r='$(($pcb_pin_radius))' fill='none' stroke='#FFCC00' stroke-width='$(($pcb_pin_width))'/>"
				pcb_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='$(($pcb_1st_pin_x))' cy='$(($pcb_1st_pin_y))' r='0.001' fill='none'/>"
				pcb_pin_svg+="\n			</g>"
			fi

			i=$(($i+1))
		done

		part_fzp+="${part_connectors_fzp}"

		icon_svg="${default_xml}"
		icon_svg+="\n${default_doc}"
		icon_svg+="\n<svg width='`jq -n $icon_width/100000`in' height='`jq -n $icon_height/100000`in' version='1.1' viewBox='0 0 $(($icon_width)) $(($icon_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		icon_svg+="\n	<g id='icon'>"
		icon_svg+="\n		<g>"
		icon_svg+="\n			<rect width='$(($icon_ic_width))' height='$(($icon_ic_height))' fill='#333333'/>"
		icon_svg+="\n			<text x='$(($icon_ic_name_x))' y='$(($icon_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		icon_svg+="\n			<path transform='translate(0,$(($icon_ic_notch_y)))' d='${notch}'/>"
		icon_svg+="\n			<circle cx='$(($icon_1st_pin_x))' cy='$(($icon_1st_pin_y))' r='$(($icon_1st_pin_radius))'/>"
		icon_svg+="\n		</g>"
		icon_svg+="\n		<g>"
		icon_svg+="\n			<polygon id='pin-template' fill='#888888' points='$(($icon_1st_pin_x-2500)),$(($icon_height-$icon_pin_size)) $(($icon_1st_pin_x-2500)),$(($icon_height-$icon_pin_size+1*$multiply)) $(($icon_1st_pin_x-500)),$(($icon_height-$icon_pin_size+2*$multiply)) $(($icon_1st_pin_x-500)),$(($icon_height)) $(($icon_1st_pin_x+500)),$(($icon_height)) $(($icon_1st_pin_x+500)),$(($icon_height-$icon_pin_size+2*$multiply)) $(($icon_1st_pin_x+2500)),$(($icon_height-$icon_pin_size+1*$multiply)) $(($icon_1st_pin_x+2500)),$(($icon_height-$icon_pin_size))'/>"
		i="1"; while [ "${i}" -lt "3" ]; do
		icon_svg+="\n			<use x='$(($i*$icon_pin_interval))' xlink:href='#pin-template'/>"
		i=$(($i+1)); done
		icon_svg+="\n		</g>"
		icon_svg+="\n	</g>"
		icon_svg+="\n</svg>"

		breadboard_svg="${default_xml}"
		breadboard_svg+="\n${default_doc}"
		breadboard_svg+="\n<svg width='`jq -n $breadboard_width/100000`in' height='`jq -n $breadboard_height/100000`in' version='1.1' viewBox='0 0 $(($breadboard_width)) $(($breadboard_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		breadboard_svg+="\n	<g id='breadboard'>"
		breadboard_svg+="\n		<g>"
		breadboard_svg+="\n			<rect width='$(($breadboard_ic_width))' height='$(($breadboard_ic_height))' fill='#333333'/>"
		breadboard_svg+="\n			<text x='$(($breadboard_ic_name_x))' y='$(($breadboard_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='${name_font_size}' text-anchor='middle'>${ic_name}</text>"
		breadboard_svg+="\n			<path transform='translate(0,$(($breadboard_ic_notch_y)))' d='${notch}'/>"
		breadboard_svg+="\n			<circle cx='$(($breadboard_1st_pin_x))' cy='$(($breadboard_1st_pin_y))' r='$(($breadboard_1st_pin_radius))'/>"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g>"
		breadboard_svg+="${breadboard_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n	</g>"
		breadboard_svg+="\n</svg>"

		schematic_svg="${default_xml}"
		schematic_svg+="\n${default_doc}"
		schematic_svg+="\n<svg width='`jq -n $schematic_width/100000`in' height='`jq -n $schematic_height/100000`in' version='1.1' viewBox='0 0 $(($schematic_width)) $(($schematic_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		schematic_svg+="\n	<g id='schematic'>"
		schematic_svg+="\n		<g>"
		schematic_svg+="\n			<rect width='$(($schematic_ic_width))' height='$(($schematic_ic_height))' fill='#FFFFFF' stroke='#000000' stroke-width='$(($multiply))'/>"
		schematic_svg+="\n			<text x='$(($schematic_ic_name_x))' y='$(($schematic_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		schematic_svg+="\n			<circle cx='$(($schematic_1st_pin_x))' cy='$(($schematic_1st_pin_y))' r='$(($schematic_1st_pin_radius))'/>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g transform='rotate(-90)' fill='#888888' font-family='OcrA' font-size='$(($pin_font_size))' text-anchor='middle'>"
		schematic_svg+="\n			<g transform='translate($((-$schematic_height+$schematic_pin_size/2)),$(($schematic_pin_text_offset)))'>"
		schematic_svg+="${schematic_text_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g stroke-width='$(($multiply))'>"
		schematic_svg+="\n			<g>"
		schematic_svg+="${schematic_pin_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n	</g>"
		schematic_svg+="\n</svg>"

		pcb_svg="${default_xml}"
		pcb_svg+="\n${default_doc}"
		pcb_svg+="\n<svg width='`jq -n $pcb_width/100000`in' height='`jq -n $pcb_height/100000`in' version='1.1' viewBox='0 0 $(($pcb_width)) $(($pcb_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		pcb_svg+="\n	<g id='silkscreen'>"
		pcb_svg+="\n		<rect width='$(($pcb_ic_width))' height='$(($pcb_ic_height))' fill='none' stroke='#000000' stroke-width='$(($multiply))'/>"
		pcb_svg+="\n		<text x='$(($pcb_ic_name_x))' y='$(($pcb_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		pcb_svg+="\n		<g transform='rotate(-90)' font-family='OcrA' font-size='$(($pin_font_size))'>"
		pcb_svg+="\n			<g>"
		pcb_svg+="${pcb_text_svg}"
		pcb_svg+="\n			</g>"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		pcb_svg+="\n	<g id='copper1'>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+="<g id='copper0'>"
		fi
		pcb_svg+="\n		<g>"
		pcb_svg+="${pcb_pin_svg}"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+='</g>'
		fi
		pcb_svg+="\n</svg>"
	elif [ "${ic_type}" = "DIP" ]; then
		icon_pin_size=$((10*$multiply/2))
		icon_pin_interval=$((10*$multiply))
		icon_ic_width=$((3*$icon_pin_interval))
		icon_ic_height=$((20*$multiply))
		icon_ic_notch_y=$(($icon_ic_height/2-5*$multiply))
		icon_ic_name_x=$(($notch_x))
		icon_ic_name_y=$(($name_font_size*3/10+$icon_ic_height/2))
		icon_1st_pin_x=$(($icon_pin_interval/2))
		icon_1st_pin_y=$(($icon_ic_height-$icon_ic_height/8))
		icon_1st_pin_radius=$((1*$multiply))
		icon_width=$(($icon_ic_width))
		icon_height=$(($icon_ic_height+$icon_pin_size*2))

		breadboard_pin_size=$((10*$multiply/2))
		breadboard_pin_interval=$((10*$multiply))
		breadboard_ic_width=$(($length/2*$breadboard_pin_interval))
		breadboard_ic_height=$((10*($ic_size-1)*$multiply))
		breadboard_ic_notch_y=$(($breadboard_ic_height/2-5*$multiply))
		breadboard_ic_name_x=$(($breadboard_ic_width/2))
		breadboard_ic_name_y=$(($name_font_size*3/10+$breadboard_ic_height/2))
		breadboard_1st_pin_x=$(($breadboard_pin_interval/2))
		breadboard_1st_pin_y=$(($breadboard_ic_height-$breadboard_ic_height/8))
		breadboard_1st_pin_radius=$((1*$multiply))
		breadboard_width=$(($breadboard_ic_width))
		breadboard_height=$(($breadboard_ic_height+$breadboard_pin_size*2))

		schematic_pin_text_offset=$((4*$multiply))
		schematic_pin_size=$((20*$multiply))
		schematic_pin_interval=$((10*$multiply))
		schematic_ic_width=$(($length/2*$schematic_pin_interval))
		schematic_ic_height=$((20*$multiply))
		schematic_ic_name_x=$(($schematic_ic_width/2))
		schematic_ic_name_y=$(($name_font_size*3/10+$schematic_ic_height/2))
		schematic_1st_pin_x=$(($schematic_pin_interval/2))
		schematic_1st_pin_y=$(($schematic_ic_height-$schematic_ic_height/8))
		schematic_1st_pin_radius=$((1*$multiply))
		schematic_width=$(($schematic_ic_width))
		schematic_height=$(($schematic_ic_height+$schematic_pin_size*2))

		pcb_pin_text_offset=$((6*$multiply))
		pcb_pin_radius=$((3*$multiply))
		pcb_pin_width=$((15*$multiply/10))
		pcb_pin_size=$((10*$multiply))
		pcb_pin_interval=$((10*$multiply))
		pcb_ic_width=$(($length/2*$pcb_pin_interval))
		pcb_ic_height=$((10*($ic_size+1)*$multiply))
		pcb_ic_name_x=$(($pcb_ic_width/2))
		pcb_ic_name_y=$(($name_font_size*3/10+$pcb_ic_height/2))
		pcb_1st_pin_x=$(($pcb_pin_interval/2))
		pcb_1st_pin_y=$(($pcb_ic_height-$pcb_pin_size/2))
		pcb_width=$(($pcb_ic_width))
		pcb_height=$(($pcb_ic_height))

		part_connectors_fzp=''
		breadboard_b_pin_svg=''
		breadboard_t_pin_svg=''
		schematic_b_text_svg=''
		schematic_t_text_svg=''
		schematic_b_pin_svg=''
		schematic_t_pin_svg=''
		pcb_b_text_svg=''
		pcb_t_text_svg=''
		pcb_b_pin_svg=''
		pcb_t_pin_svg=''

		i="0"
		while [ "${i}" -lt "${length}" ]; do
			pin_label=`echo "${json}" | jq -r ".connectors[${i}].name"`
			pin_name=$(($i+1))"-${pin_label}"
			pin_description=`echo "${json}" | jq -r ".connectors[${i}].description"`
			echo "Pin $(($i+1))/$(($length)) - ${pin_label}"

			# part middle
			part_connectors_fzp+="\n		<connector id='connector-${pin_name}' name='${pin_label}' type='male'>"
			part_connectors_fzp+="\n			<description>${pin_description}</description>"
			part_connectors_fzp+="\n			<views>"
			part_connectors_fzp+="\n				<breadboardView>"
			part_connectors_fzp+="\n					<p layer='breadboard' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</breadboardView>"
			part_connectors_fzp+="\n				<schematicView>"
			part_connectors_fzp+="\n					<p layer='schematic' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</schematicView>"
			part_connectors_fzp+="\n				<pcbView>"
			part_connectors_fzp+="\n					<p layer='copper1' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
			part_connectors_fzp+="\n					<p layer='copper0' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			fi
			part_connectors_fzp+="\n				</pcbView>"
			part_connectors_fzp+="\n			</views>"
			part_connectors_fzp+="\n		</connector>"

			if [ "${i}" -gt "0" ]; then
				if [ "${i}" -lt $(($length/2)) ]; then
					# breadboard bottom pins
					breadboard_b_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$breadboard_pin_interval)),0)'>"
					breadboard_b_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					breadboard_b_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					breadboard_b_pin_svg+="\n			</g>"

					# schematic bottom text
					schematic_b_text_svg+="\n				<text x='0' y='$(($i*$schematic_pin_interval))'>${pin_name}</text>"

					# schematic bottom pins
					schematic_b_pin_svg+="\n				<g transform='translate($((($i%($length/2))*$schematic_pin_interval)),0)'>"
					schematic_b_pin_svg+="\n					<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					schematic_b_pin_svg+="\n					<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					schematic_b_pin_svg+="\n				</g>"

					# pcb bottom text
					pcb_b_text_svg+="\n				<text x='$((-$pcb_height+$pcb_pin_interval))' y='$(($i*$pcb_pin_interval+$pcb_pin_text_offset))'>${pin_label}</text>"

					# pcb bottom pins
					pcb_b_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$pcb_pin_interval)),0)'>"
					pcb_b_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					pcb_b_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					pcb_b_pin_svg+="\n			</g>"
				else
					# breadboard top pins
					breadboard_t_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$breadboard_pin_interval)),0)'>"
					breadboard_t_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					breadboard_t_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					breadboard_t_pin_svg+="\n			</g>"

					# schematic top text
					schematic_t_text_svg+="\n				<text x='0' y='$((($length-$i-1)*$schematic_pin_interval))'>${pin_label}</text>"

					# schematic top pins
					schematic_t_pin_svg+="\n				<g transform='translate($((($i%($length/2))*$schematic_pin_interval)),0)'>"
					schematic_t_pin_svg+="\n					<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					schematic_t_pin_svg+="\n					<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					schematic_t_pin_svg+="\n				</g>"

					# pcb bottom text
					pcb_t_text_svg+="\n				<text x='$((-$pcb_pin_interval))' y='$((($length-$i-1)*$pcb_pin_interval+$pcb_pin_text_offset))'>${pin_label}</text>"

					# pcb top pins
					pcb_t_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$pcb_pin_interval)),0)'>"
					pcb_t_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					pcb_t_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					pcb_t_pin_svg+="\n			</g>"
				fi
			else
				template="${pin_name}"
				# breadboard 1st pin
				breadboard_b_pin_svg+="\n			<g>"
				breadboard_b_pin_svg+="\n				<polygon id='connector-${pin_name}-pin' fill='#888888' points='$(($breadboard_1st_pin_x-2500)),$(($breadboard_height-$breadboard_pin_size)) $(($breadboard_1st_pin_x-2500)),$(($breadboard_height-$breadboard_pin_size+1*$multiply)) $(($breadboard_1st_pin_x-500)),$(($breadboard_height-$breadboard_pin_size+2*$multiply)) $(($breadboard_1st_pin_x-500)),$(($breadboard_height)) $(($breadboard_1st_pin_x+500)),$(($breadboard_height)) $(($breadboard_1st_pin_x+500)),$(($breadboard_height-$breadboard_pin_size+2*$multiply)) $(($breadboard_1st_pin_x+2500)),$(($breadboard_height-$breadboard_pin_size+1*$multiply)) $(($breadboard_1st_pin_x+2500)),$(($breadboard_height-$breadboard_pin_size))'/>"
				breadboard_b_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='5000' cy='$(($breadboard_height))' r='0.001' fill='none'/>"
				breadboard_b_pin_svg+="\n			</g>"

				# schematic bottom text
				schematic_b_text_svg+="\n				<text x='0' y='$(($i*$schematic_pin_interval))'>${pin_label}</text>"

				# schematic 1st pin
				schematic_b_pin_svg+="\n				<g>"
				schematic_b_pin_svg+="\n					<polyline id='connector-${pin_name}-pin' points='$(($schematic_1st_pin_x)),$(($schematic_height-$schematic_pin_size)) $(($schematic_1st_pin_x)),$(($schematic_height))' stroke='#888888'/>"
				schematic_b_pin_svg+="\n					<circle id='connector-${pin_name}-terminal' cx='$(($schematic_1st_pin_x))' cy='$(($schematic_height))' r='0.001' fill='none'/>"
				schematic_b_pin_svg+="\n				</g>"

				# pcb bottom text
				pcb_b_text_svg+="\n				<text x='$((-$pcb_height+$pcb_pin_interval))' y='$(($i*$pcb_pin_interval+$pcb_pin_text_offset))'>${pin_label}</text>"

				# pcb 1st pin
				pcb_b_pin_svg+="\n			<g>"
				pcb_b_pin_svg+="\n				<rect x='$(($pcb_1st_pin_x-$pcb_pin_radius))' y='$(($pcb_1st_pin_y-$pcb_pin_radius))' width='$(($pcb_pin_radius*2))' height='$(($pcb_pin_radius*2))' fill='none' stroke='#FFCC00' stroke-width='$(($pcb_pin_width))'/>"
				pcb_b_pin_svg+="\n				<circle id='connector-${pin_name}-pin' cx='$(($pcb_1st_pin_x))' cy='$(($pcb_1st_pin_y))' r='$(($pcb_pin_radius))' fill='none' stroke='#FFCC00' stroke-width='$(($pcb_pin_width))'/>"
				pcb_b_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='$(($pcb_1st_pin_x))' cy='$(($pcb_1st_pin_y))' r='0.001' fill='none'/>"
				pcb_b_pin_svg+="\n			</g>"
			fi

			i=$(($i+1))
		done

		part_fzp+="${part_connectors_fzp}"

		icon_svg="${default_xml}"
		icon_svg+="\n${default_doc}"
		icon_svg+="\n<svg width='`jq -n $icon_width/100000`in' height='`jq -n $icon_height/100000`in' version='1.1' viewBox='0 0 $(($icon_width)) $(($icon_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		icon_svg+="\n	<g id='icon'>"
		icon_svg+="\n		<g transform='translate(0,$(($icon_pin_size)))'>"
		icon_svg+="\n			<rect width='$(($icon_ic_width))' height='$(($icon_ic_height))' fill='#333333'/>"
		icon_svg+="\n			<text x='$(($icon_ic_name_x))' y='$(($icon_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='$(($name_font_size))'>${ic_name}</text>"
		icon_svg+="\n			<path transform='translate(0,$(($icon_ic_notch_y)))' d='${notch}'/>"
		icon_svg+="\n			<circle cx='$(($icon_1st_pin_x))' cy='$(($icon_1st_pin_y))' r='$(($icon_1st_pin_radius))'/>"
		icon_svg+="\n		</g>"
		icon_svg+="\n		<g>"
		icon_svg+="\n			<polygon id='pin-template' fill='#888888' points='$(($icon_1st_pin_x-2500)),$(($icon_height-$icon_pin_size)) $(($icon_1st_pin_x-2500)),$(($icon_height-$icon_pin_size+1*$multiply)) $(($icon_1st_pin_x-500)),$(($icon_height-$icon_pin_size+2*$multiply)) $(($icon_1st_pin_x-500)),$(($icon_height)) $(($icon_1st_pin_x+500)),$(($icon_height)) $(($icon_1st_pin_x+500)),$(($icon_height-$icon_pin_size+2*$multiply)) $(($icon_1st_pin_x+2500)),$(($icon_height-$icon_pin_size+1*$multiply)) $(($icon_1st_pin_x+2500)),$(($icon_height-$icon_pin_size))'/>"
		i="1"; while [ "${i}" -lt "3" ]; do
		icon_svg+="\n			<use x='$(($i*$icon_pin_interval))' xlink:href='#pin-template'/>"
		i=$(($i+1)); done
		icon_svg+="\n		</g>"
		icon_svg+="\n		<g transform='rotate(180 $(($icon_width/2)),$(($icon_height/2)))'>"
		i="0"; while [ "${i}" -lt "3" ]; do
		icon_svg+="\n			<use x='$(($i*$icon_pin_interval))' xlink:href='#pin-template'/>"
		i=$(($i+1)); done
		icon_svg+="\n		</g>"
		icon_svg+="\n	</g>"
		icon_svg+="\n</svg>"

		breadboard_svg="${default_xml}"
		breadboard_svg+="\n${default_doc}"
		breadboard_svg+="\n<svg width='`jq -n $breadboard_width/100000`in' height='`jq -n $breadboard_height/100000`in' version='1.1' viewBox='0 0 $(($breadboard_width)) $(($breadboard_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		breadboard_svg+="\n	<g id='breadboard'>"
		breadboard_svg+="\n		<g transform='translate(0,$(($breadboard_pin_size)))'>"
		breadboard_svg+="\n			<rect width='$(($breadboard_ic_width))' height='$(($breadboard_ic_height))' fill='#333333'/>"
		breadboard_svg+="\n			<text x='$(($breadboard_ic_name_x))' y='$(($breadboard_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='${name_font_size}' text-anchor='middle'>${ic_name}</text>"
		breadboard_svg+="\n			<path transform='translate(0,$(($breadboard_ic_notch_y)))' d='${notch}'/>"
		breadboard_svg+="\n			<circle cx='$(($breadboard_1st_pin_x))' cy='$(($breadboard_1st_pin_y))' r='$(($breadboard_1st_pin_radius))'/>"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g>"
		breadboard_svg+="${breadboard_b_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g transform='rotate(180 $(($breadboard_width/2)),$(($breadboard_height/2)))'>"
		breadboard_svg+="${breadboard_t_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n	</g>"
		breadboard_svg+="\n</svg>"

		schematic_svg="${default_xml}"
		schematic_svg+="\n${default_doc}"
		schematic_svg+="\n<svg width='`jq -n $schematic_width/100000`in' height='`jq -n $schematic_height/100000`in' version='1.1' viewBox='0 0 $(($schematic_width)) $(($schematic_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		schematic_svg+="\n	<g id='schematic'>"
		schematic_svg+="\n		<g transform='translate(0,$(($schematic_pin_size)))'>"
		schematic_svg+="\n			<rect width='$(($schematic_ic_width))' height='$(($schematic_ic_height))' fill='#FFFFFF' stroke='#000000' stroke-width='$(($multiply))'/>"
		schematic_svg+="\n			<text x='$(($schematic_ic_name_x))' y='$(($schematic_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		schematic_svg+="\n			<circle cx='$(($schematic_1st_pin_x))' cy='$(($schematic_1st_pin_y))' r='$(($schematic_1st_pin_radius))'/>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g transform='rotate(-90)' fill='#888888' font-family='OcrA' font-size='$(($pin_font_size))' text-anchor='middle'>"
		schematic_svg+="\n			<g transform='translate($((-$schematic_height+$schematic_pin_size/2)),$(($schematic_pin_text_offset)))'>"
		schematic_svg+="${schematic_b_text_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n			<g transform='translate($((-$schematic_pin_size/2)),$(($schematic_pin_text_offset)))'>"
		schematic_svg+="${schematic_t_text_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g stroke-width='$(($multiply))'>"
		schematic_svg+="\n			<g>"
		schematic_svg+="${schematic_b_pin_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n			<g transform='rotate(180 $(($schematic_width/2)),$(($schematic_height/2)))'>"
		schematic_svg+="${schematic_t_pin_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n	</g>"
		schematic_svg+="\n</svg>"

		pcb_svg="${default_xml}"
		pcb_svg+="\n${default_doc}"
		pcb_svg+="\n<svg width='`jq -n $pcb_width/100000`in' height='`jq -n $pcb_height/100000`in' version='1.1' viewBox='0 0 $(($pcb_width)) $(($pcb_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		pcb_svg+="\n	<g id='silkscreen'>"
		pcb_svg+="\n		<rect width='$(($pcb_ic_width))' height='$(($pcb_ic_height))' fill='none' stroke='#000000' stroke-width='$(($multiply))'/>"
		pcb_svg+="\n		<text x='$(($pcb_ic_name_x))' y='$(($pcb_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		pcb_svg+="\n		<g transform='rotate(-90)' font-family='OcrA' font-size='$(($pin_font_size))'>"
		pcb_svg+="\n			<g>"
		pcb_svg+="${pcb_b_text_svg}"
		pcb_svg+="\n			</g>"
		pcb_svg+="\n			<g text-anchor='end'>"
		pcb_svg+="${pcb_t_text_svg}"
		pcb_svg+="\n			</g>"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		pcb_svg+="\n	<g id='copper1'>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+="<g id='copper0'>"
		fi
		pcb_svg+="\n		<g>"
		pcb_svg+="${pcb_b_pin_svg}"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n		<g transform='rotate(180 $(($pcb_width/2)),$(($pcb_height/2)))'>"
		pcb_svg+="${pcb_t_pin_svg}"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+='</g>'
		fi
		pcb_svg+="\n</svg>"
	elif [ "${ic_type}" = "SOP" ]; then
		icon_pin_width=$((1*$multiply))
		icon_pin_size=$((10*$multiply/2))
		icon_pin_interval=$((10*$multiply/2))
		icon_ic_width=$((4*$icon_pin_interval))
		icon_ic_height=$((10*$multiply))
		icon_ic_name_x=$(($icon_ic_width/2))
		icon_ic_name_y=$(($name_font_size*3/10+$icon_ic_height/2))
		icon_1st_pin_x=$(($icon_pin_interval/2))
		icon_1st_pin_y=$(($icon_ic_height-$icon_ic_height/8))
		icon_1st_pin_radius=$((1*$multiply/2))
		icon_width=$(($icon_ic_width))
		icon_height=$(($icon_ic_height+$icon_pin_size*2))

		breadboard_pin_width=$((1*$multiply))
		breadboard_adapter_pin_size=$((1*$multiply))
		breadboard_adapter_pin_interval=$((10*$multiply))
		breadboard_pin_size=$((10*$multiply/2))
		breadboard_pin_interval=$((10*$multiply/2))
		breadboard_ic_width=$(($length/2*$breadboard_pin_interval))
		breadboard_ic_height=$((10*$multiply))
		breadboard_ic_name_x=$(($breadboard_ic_width/2))
		breadboard_ic_name_y=$(($name_font_size*3/10+$breadboard_ic_height/2))
		breadboard_1st_pin_x=$(($breadboard_pin_interval/2))
		breadboard_1st_pin_y=$(($breadboard_ic_height-$breadboard_ic_height/8))
		breadboard_1st_pin_radius=$((1*$multiply/2))
		breadboard_width=$(($length/2*$breadboard_adapter_pin_interval))
		breadboard_height=$((40*$multiply))
		breadboard_pin_x=$((($breadboard_width-$breadboard_ic_width)/2+$breadboard_1st_pin_x-$breadboard_pin_width/2))
		breadboard_pin_y=$((($breadboard_height+$breadboard_ic_height)/2))
		breadboard_1st_adapter_pin_x=$((5*$multiply))
		breadboard_1st_adapter_pin_y=$(($breadboard_height-5*$multiply))
		breadboard_1st_adapter_pin_width=$((1*$multiply))

		schematic_pin_text_offset=$((4*$multiply))
		schematic_pin_size=$((20*$multiply/2))
		schematic_pin_interval=$((10*$multiply))
		schematic_ic_width=$(($length/2*$schematic_pin_interval))
		schematic_ic_height=$((20*$multiply))
		schematic_ic_name_x=$(($schematic_ic_width/2))
		schematic_ic_name_y=$(($name_font_size*3/10+$schematic_ic_height/2))
		schematic_1st_pin_x=$(($schematic_pin_interval/2))
		schematic_1st_pin_y=$(($schematic_ic_height-$schematic_ic_height/8))
		schematic_1st_pin_radius=$((1*$multiply/2))
		schematic_width=$(($schematic_ic_width))
		schematic_height=$(($schematic_ic_height+$schematic_pin_size*2))

		pcb_pin_width=$((2*$multiply))
		pcb_pin_size=$((10*$multiply/2))
		pcb_pin_interval=$((10*$multiply/2))
		pcb_ic_width=$(($length/2*$pcb_pin_interval))
		pcb_ic_height=$((10*$multiply))
		pcb_ic_name_x=$(($pcb_ic_width/2))
		pcb_ic_name_y=$(($name_font_size*3/10+$pcb_ic_height/2))
		pcb_1st_pin_x=$(($pcb_pin_interval/2))
		pcb_1st_pin_y=$(($pcb_ic_height-$pcb_pin_size/2))
		pcb_1st_pin_radius=$((1*$multiply/2))
		pcb_width=$(($pcb_ic_width))
		pcb_height=$(($pcb_ic_height+$pcb_pin_size*2))

		part_connectors_fzp=''
		breadboard_b_pin_svg=''
		breadboard_t_pin_svg=''
		breadboard_b_virtual_pin_svg=''
		breadboard_t_virtual_pin_svg=''
		schematic_b_text_svg=''
		schematic_t_text_svg=''
		schematic_b_pin_svg=''
		schematic_t_pin_svg=''
		pcb_b_text_svg=''
		pcb_t_text_svg=''
		pcb_b_pin_svg=''
		pcb_t_pin_svg=''

		i="0"
		while [ "${i}" -lt "${length}" ]; do
			pin_label=`echo "${json}" | jq -r ".connectors[${i}].name"`
			pin_name=$(($i+1))"-${pin_label}"
			pin_description=`echo "${json}" | jq -r ".connectors[${i}].description"`
			echo "Pin $(($i+1))/$(($length)) - ${pin_label}"

			# part middle
			part_connectors_fzp+="\n		<connector id='connector-${pin_name}' name='${pin_label}' type='male'>"
			part_connectors_fzp+="\n			<description>${pin_description}</description>"
			part_connectors_fzp+="\n			<views>"
			part_connectors_fzp+="\n				<breadboardView>"
			part_connectors_fzp+="\n					<p layer='breadboard' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</breadboardView>"
			part_connectors_fzp+="\n				<schematicView>"
			part_connectors_fzp+="\n					<p layer='schematic' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</schematicView>"
			part_connectors_fzp+="\n				<pcbView>"
			part_connectors_fzp+="\n					<p layer='copper1' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
			part_connectors_fzp+="\n					<p layer='copper0' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			fi
			part_connectors_fzp+="\n				</pcbView>"
			part_connectors_fzp+="\n			</views>"
			part_connectors_fzp+="\n		</connector>"

			if [ "${i}" -gt "0" ]; then
				if [ "${i}" -lt $(($length/2)) ]; then
					# breadboard bottom pins
					breadboard_b_pin_svg+="\n			<g>"
					breadboard_b_pin_svg+="\n				<rect transform='translate($((($i%($length/2))*$breadboard_pin_interval)),0)' fill='#888888' x='$(($breadboard_pin_x))' y='$(($breadboard_pin_y))' width='$(($breadboard_pin_width))' height='$(($breadboard_pin_size))'/>"
					breadboard_b_pin_svg+="\n			</g>"

					# breadboard bottom adapter pins
					breadboard_b_virtual_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$breadboard_adapter_pin_interval)),0)'>"
					breadboard_b_virtual_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					breadboard_b_virtual_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					breadboard_b_virtual_pin_svg+="\n			</g>"

					# schematic bottom text
					schematic_b_text_svg+="\n				<text x='0' y='$(($i*$schematic_pin_interval))'>${pin_label}</text>"

					# schematic bottom pins
					schematic_b_pin_svg+="\n				<g transform='translate($((($i%($length/2))*$schematic_pin_interval)),0)'>"
					schematic_b_pin_svg+="\n					<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					schematic_b_pin_svg+="\n					<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					schematic_b_pin_svg+="\n				</g>"

					# pcb bottom pins
					pcb_b_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$pcb_pin_interval)),0)'>"
					pcb_b_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					pcb_b_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					pcb_b_pin_svg+="\n			</g>"
				else
					# breadboard top pins
					breadboard_t_pin_svg+="\n			<g>"
					breadboard_t_pin_svg+="\n				<rect transform='translate($((($i%($length/2))*$breadboard_pin_interval)),0)' fill='#888888' x='$(($breadboard_pin_x))' y='$(($breadboard_pin_y))' width='$(($breadboard_pin_width))' height='$(($breadboard_pin_size))'/>"
					breadboard_t_pin_svg+="\n			</g>"

					# breadboard top adapter pins
					breadboard_t_virtual_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$breadboard_adapter_pin_interval)),0)'>"
					breadboard_t_virtual_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					breadboard_t_virtual_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					breadboard_t_virtual_pin_svg+="\n			</g>"

					# schematic top text
					schematic_t_text_svg+="\n				<text x='0' y='$((($length-$i-1)*$schematic_pin_interval))'>${pin_label}</text>"

					# schematic top pins
					schematic_t_pin_svg+="\n				<g transform='translate($((($i%($length/2))*$schematic_pin_interval)),0)'>"
					schematic_t_pin_svg+="\n					<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					schematic_t_pin_svg+="\n					<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					schematic_t_pin_svg+="\n				</g>"

					# pcb top pins
					pcb_t_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$pcb_pin_interval)),0)'>"
					pcb_t_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					pcb_t_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					pcb_t_pin_svg+="\n			</g>"
				fi
			else
				template="${pin_name}"
				# breadboard 1st pin
				breadboard_b_pin_svg+="\n			<g>"
				breadboard_b_pin_svg+="\n				<rect transform='translate(0,0)' fill='#888888' x='$(($breadboard_pin_x))' y='$(($breadboard_pin_y))' width='$(($breadboard_pin_width))' height='$(($breadboard_pin_size))'/>"
				breadboard_b_pin_svg+="\n			</g>"

				# breadboard 1st adpater pin
				breadboard_b_virtual_pin_svg+="\n			<g>"
				breadboard_b_virtual_pin_svg+="\n				<circle id='connector-${pin_name}-pin' cx='$(($breadboard_1st_adapter_pin_x))' cy='$(($breadboard_1st_adapter_pin_y))' r='$(($breadboard_1st_adapter_pin_width))' fill='#FFCC00'/>"
				breadboard_b_virtual_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='$(($breadboard_1st_adapter_pin_x))' cy='$(($breadboard_1st_adapter_pin_y))' r='0.001'/>"
				breadboard_b_virtual_pin_svg+="\n			</g>"

				# schematic bottom text
				schematic_b_text_svg+="\n				<text x='0' y='$(($i*$schematic_pin_interval))'>${pin_label}</text>"

				# schematic 1st pin
				schematic_b_pin_svg+="\n				<g>"
				schematic_b_pin_svg+="\n					<polyline id='connector-${pin_name}-pin' points='$(($schematic_1st_pin_x)),$(($schematic_height-$schematic_pin_size)) $(($schematic_1st_pin_x)),$(($schematic_height))' stroke='#888888'/>"
				schematic_b_pin_svg+="\n					<circle id='connector-${pin_name}-terminal' cx='$(($schematic_1st_pin_x))' cy='$(($schematic_height))' r='0.001' fill='none'/>"
				schematic_b_pin_svg+="\n				</g>"

				# pcb 1st pin
				pcb_b_pin_svg+="\n			<g>"
				pcb_b_pin_svg+="\n				<rect id='connector-${pin_name}-pin' x='$(($pcb_1st_pin_x-$pcb_pin_width/2))' y='$(($pcb_height-$pcb_pin_size))' width='$(($pcb_pin_width))' height='$(($pcb_pin_size))' fill='#FFCC00'/>"
				pcb_b_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='$(($pcb_1st_pin_x))' cy='$(($pcb_height))' r='0.001' fill='none'/>"
				pcb_b_pin_svg+="\n			</g>"
			fi

			i=$(($i+1))
		done

		part_fzp+="${part_connectors_fzp}"

		icon_svg="${default_xml}"
		icon_svg+="\n${default_doc}"
		icon_svg+="\n<svg width='`jq -n $icon_width/100000`in' height='`jq -n $icon_height/100000`in' version='1.1' viewBox='0 0 $(($icon_width)) $(($icon_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		icon_svg+="\n	<g id='icon'>"
		icon_svg+="\n		<g transform='translate(0,$(($icon_pin_size)))'>"
		icon_svg+="\n			<rect width='$(($icon_ic_width))' height='$(($icon_ic_height))' fill='#333333'/>"
		icon_svg+="\n			<text x='$(($icon_ic_name_x))' y='$(($icon_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		icon_svg+="\n			<circle cx='$(($icon_1st_pin_x))' cy='$(($icon_1st_pin_y))' r='$(($icon_1st_pin_radius))'/>"
		icon_svg+="\n		</g>"
		icon_svg+="\n		<g>"
		icon_svg+="\n			<rect id='pin-template' fill='#888888' x='$(($icon_1st_pin_x-$icon_pin_width/2))' y='$(($icon_height-$icon_pin_size))' width='$(($icon_pin_width))' height='$(($icon_pin_size))'/>"
		i="1"; while [ "${i}" -lt "4" ]; do
		icon_svg+="\n			<use x='$(($i*$icon_pin_interval))' xlink:href='#pin-template'/>"
		i=$(($i+1)); done
		icon_svg+="\n		</g>"
		icon_svg+="\n		<g transform='rotate(180 $(($icon_width/2)),$(($icon_height/2)))'>"
		i="0"; while [ "${i}" -lt "4" ]; do
		icon_svg+="\n			<use x='$(($i*$icon_pin_interval))' xlink:href='#pin-template'/>"
		i=$(($i+1)); done
		icon_svg+="\n		</g>"
		icon_svg+="\n	</g>"
		icon_svg+="\n</svg>"

		breadboard_svg="${default_xml}"
		breadboard_svg+="\n${default_doc}"
		breadboard_svg+="\n<svg width='`jq -n $breadboard_width/100000`in' height='`jq -n $breadboard_height/100000`in' version='1.1' viewBox='0 0 $(($breadboard_width)) $(($breadboard_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		breadboard_svg+="\n	<g id='breadboard'>"
		breadboard_svg+="\n		<rect width='$(($breadboard_width))' height='$(($breadboard_height))' fill='#004400'/>"
		breadboard_svg+="\n		<g transform='translate($((($breadboard_width-$breadboard_ic_width)/2)),$((($breadboard_height-$breadboard_ic_height)/2)))'>"
		breadboard_svg+="\n			<rect width='$(($breadboard_ic_width))' height='$(($breadboard_ic_height))' fill='#333333'/>"
		breadboard_svg+="\n			<text x='$(($breadboard_ic_name_x))' y='$(($breadboard_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='${name_font_size}' text-anchor='middle'>${ic_name}</text>"
		breadboard_svg+="\n			<circle cx='$(($breadboard_1st_pin_x))' cy='$(($breadboard_1st_pin_y))' r='$(($breadboard_1st_pin_radius))'/>"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g>"
		breadboard_svg+="${breadboard_b_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g transform='rotate(180 $(($breadboard_width/2)),$(($breadboard_height/2)))'>"
		breadboard_svg+="${breadboard_t_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g>"
		breadboard_svg+="${breadboard_b_virtual_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g transform='rotate(180 $(($breadboard_width/2)),$(($breadboard_height/2)))'>"
		breadboard_svg+="${breadboard_t_virtual_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n	</g>"
		breadboard_svg+="\n</svg>"

		schematic_svg="${default_xml}"
		schematic_svg+="\n${default_doc}"
		schematic_svg+="\n<svg width='`jq -n $schematic_width/100000`in' height='`jq -n $schematic_height/100000`in' version='1.1' viewBox='0 0 $(($schematic_width)) $(($schematic_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		schematic_svg+="\n	<g id='schematic'>"
		schematic_svg+="\n		<g transform='translate(0,$(($schematic_pin_size)))'>"
		schematic_svg+="\n			<rect width='$(($schematic_ic_width))' height='$(($schematic_ic_height))' fill='#FFFFFF' stroke='#000000' stroke-width='$(($multiply))'/>"
		schematic_svg+="\n			<text x='$(($schematic_ic_name_x))' y='$(($schematic_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		schematic_svg+="\n			<circle cx='$(($schematic_1st_pin_x))' cy='$(($schematic_1st_pin_y))' r='$(($schematic_1st_pin_radius))'/>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g transform='rotate(-90)' fill='#888888' font-family='OcrA' font-size='$(($pin_font_size))' text-anchor='middle'>"
		schematic_svg+="\n			<g transform='translate($((-$schematic_height+$schematic_pin_size/2)),$(($schematic_pin_text_offset)))'>"
		schematic_svg+="${schematic_b_text_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n			<g transform='translate($((-$schematic_pin_size/2)),$(($schematic_pin_text_offset)))'>"
		schematic_svg+="${schematic_t_text_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g stroke-width='$(($multiply))'>"
		schematic_svg+="\n			<g>"
		schematic_svg+="${schematic_b_pin_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n			<g transform='rotate(180 $(($schematic_width/2)),$(($schematic_height/2)))'>"
		schematic_svg+="${schematic_t_pin_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n	</g>"
		schematic_svg+="\n</svg>"

		pcb_svg="${default_xml}"
		pcb_svg+="\n${default_doc}"
		pcb_svg+="\n<svg width='`jq -n $pcb_width/100000`in' height='`jq -n $pcb_height/100000`in' version='1.1' viewBox='0 0 $(($pcb_width)) $(($pcb_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		pcb_svg+="\n	<g id='silkscreen'>"
		pcb_svg+="\n		<g transform='translate(0,$(($pcb_pin_size)))'>"
		pcb_svg+="\n			<rect width='$(($pcb_ic_width))' height='$(($pcb_ic_height))' fill='none' stroke='#000000' stroke-width='$(($multiply))'/>"
		pcb_svg+="\n			<circle cx='$(($pcb_1st_pin_x))' cy='$(($pcb_1st_pin_y))' r='$(($pcb_1st_pin_radius))'/>"
		pcb_svg+="\n			<text x='$(($pcb_ic_name_x))' y='$(($pcb_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		pcb_svg+="\n	<g id='copper1'>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+="<g id='copper0'>"
		fi
		pcb_svg+="\n		<g>"
		pcb_svg+="${pcb_b_pin_svg}"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n		<g transform='rotate(180 $(($pcb_width/2)),$(($pcb_height/2)))'>"
		pcb_svg+="${pcb_t_pin_svg}"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+='</g>'
		fi
		pcb_svg+="\n</svg>"
	elif [ "${ic_type}" = "SSOP" ]; then
		icon_pin_width=$((1*$multiply))
		icon_pin_size=$((10*$multiply/2))
		icon_pin_interval=$((10*$multiply/4))
		icon_ic_width=$((8*$icon_pin_interval))
		icon_ic_height=$((10*$multiply))
		icon_ic_name_x=$(($icon_ic_width/2))
		icon_ic_name_y=$(($name_font_size*3/10+$icon_ic_height/2))
		icon_1st_pin_x=$(($icon_pin_interval/2))
		icon_1st_pin_y=$(($icon_ic_height-$icon_ic_height/8))
		icon_1st_pin_radius=$((1*$multiply/2))
		icon_width=$(($icon_ic_width))
		icon_height=$(($icon_ic_height+$icon_pin_size*2))

		breadboard_pin_width=$((1*$multiply))
		breadboard_adapter_pin_size=$((1*$multiply))
		breadboard_adapter_pin_interval=$((10*$multiply))
		breadboard_pin_size=$((10*$multiply/2))
		breadboard_pin_interval=$((10*$multiply/4))
		breadboard_ic_width=$(($length/2*$breadboard_pin_interval))
		breadboard_ic_height=$((10*$multiply))
		breadboard_ic_name_x=$(($breadboard_ic_width/2))
		breadboard_ic_name_y=$(($name_font_size*3/10+$breadboard_ic_height/2))
		breadboard_1st_pin_x=$(($breadboard_pin_interval/2))
		breadboard_1st_pin_y=$(($breadboard_ic_height-$breadboard_ic_height/8))
		breadboard_1st_pin_radius=$((1*$multiply/2))
		breadboard_width=$(($length/2*$breadboard_adapter_pin_interval))
		breadboard_height=$((40*$multiply))
		breadboard_pin_x=$((($breadboard_width-$breadboard_ic_width)/2+$breadboard_1st_pin_x-$breadboard_pin_width/2))
		breadboard_pin_y=$((($breadboard_height+$breadboard_ic_height)/2))
		breadboard_1st_adapter_pin_x=$((5*$multiply))
		breadboard_1st_adapter_pin_y=$(($breadboard_height-5*$multiply))
		breadboard_1st_adapter_pin_width=$((1*$multiply))

		schematic_pin_text_offset=$((4*$multiply))
		schematic_pin_size=$((20*$multiply/2))
		schematic_pin_interval=$((10*$multiply/2))
		schematic_ic_width=$(($length/2*$schematic_pin_interval))
		schematic_ic_height=$((20*$multiply))
		schematic_ic_name_x=$(($schematic_ic_width/2))
		schematic_ic_name_y=$(($name_font_size*3/10+$schematic_ic_height/2))
		schematic_1st_pin_x=$(($schematic_pin_interval/2))
		schematic_1st_pin_y=$(($schematic_ic_height-$schematic_ic_height/8))
		schematic_1st_pin_radius=$((1*$multiply/2))
		schematic_width=$(($schematic_ic_width))
		schematic_height=$(($schematic_ic_height+$schematic_pin_size*2))

		pcb_pin_width=$((2*$multiply/2))
		pcb_pin_size=$((10*$multiply/2))
		pcb_pin_interval=$((10*$multiply/4))
		pcb_ic_width=$(($length/2*$pcb_pin_interval))
		pcb_ic_height=$((10*$multiply))
		pcb_ic_name_x=$(($pcb_ic_width/2))
		pcb_ic_name_y=$(($name_font_size*3/10+$pcb_ic_height/2))
		pcb_1st_pin_x=$(($pcb_pin_interval/2))
		pcb_1st_pin_y=$(($pcb_ic_height-$pcb_pin_size/2))
		pcb_1st_pin_radius=$((1*$multiply/2))
		pcb_width=$(($pcb_ic_width))
		pcb_height=$(($pcb_ic_height+$pcb_pin_size*2))

		part_connectors_fzp=''
		breadboard_b_pin_svg=''
		breadboard_t_pin_svg=''
		breadboard_b_virtual_pin_svg=''
		breadboard_t_virtual_pin_svg=''
		schematic_b_text_svg=''
		schematic_t_text_svg=''
		schematic_b_pin_svg=''
		schematic_t_pin_svg=''
		pcb_b_text_svg=''
		pcb_t_text_svg=''
		pcb_b_pin_svg=''
		pcb_t_pin_svg=''

		i="0"
		while [ "${i}" -lt "${length}" ]; do
			pin_label=`echo "${json}" | jq -r ".connectors[${i}].name"`
			pin_name=$(($i+1))"-${pin_label}"
			pin_description=`echo "${json}" | jq -r ".connectors[${i}].description"`
			echo "Pin $(($i+1))/$(($length)) - ${pin_label}"

			# part middle
			part_connectors_fzp+="\n		<connector id='connector-${pin_name}' name='${pin_label}' type='male'>"
			part_connectors_fzp+="\n			<description>${pin_description}</description>"
			part_connectors_fzp+="\n			<views>"
			part_connectors_fzp+="\n				<breadboardView>"
			part_connectors_fzp+="\n					<p layer='breadboard' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</breadboardView>"
			part_connectors_fzp+="\n				<schematicView>"
			part_connectors_fzp+="\n					<p layer='schematic' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			part_connectors_fzp+="\n				</schematicView>"
			part_connectors_fzp+="\n				<pcbView>"
			part_connectors_fzp+="\n					<p layer='copper1' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
			part_connectors_fzp+="\n					<p layer='copper0' svgId='connector-${pin_name}-pin' terminalId='connector-${pin_name}-terminal'/>"
			fi
			part_connectors_fzp+="\n				</pcbView>"
			part_connectors_fzp+="\n			</views>"
			part_connectors_fzp+="\n		</connector>"

			if [ "${i}" -gt "0" ]; then
				if [ "${i}" -lt $(($length/2)) ]; then
					# breadboard bottom pins
					breadboard_b_pin_svg+="\n			<g>"
					breadboard_b_pin_svg+="\n				<rect transform='translate($((($i%($length/2))*$breadboard_pin_interval)),0)' fill='#888888' x='$(($breadboard_pin_x))' y='$(($breadboard_pin_y))' width='$(($breadboard_pin_width))' height='$(($breadboard_pin_size))'/>"
					breadboard_b_pin_svg+="\n			</g>"

					# breadboard bottom adapter pins
					breadboard_b_virtual_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$breadboard_adapter_pin_interval)),0)'>"
					breadboard_b_virtual_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					breadboard_b_virtual_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					breadboard_b_virtual_pin_svg+="\n			</g>"

					# schematic bottom text
					schematic_b_text_svg+="\n				<text x='0' y='$(($i*$schematic_pin_interval))'>${pin_label}</text>"

					# schematic bottom pins
					schematic_b_pin_svg+="\n				<g transform='translate($((($i%($length/2))*$schematic_pin_interval)),0)'>"
					schematic_b_pin_svg+="\n					<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					schematic_b_pin_svg+="\n					<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					schematic_b_pin_svg+="\n				</g>"

					# pcb bottom pins
					pcb_b_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$pcb_pin_interval)),0)'>"
					pcb_b_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					pcb_b_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					pcb_b_pin_svg+="\n			</g>"
				else
					# breadboard top pins
					breadboard_t_pin_svg+="\n			<g>"
					breadboard_t_pin_svg+="\n				<rect transform='translate($((($i%($length/2))*$breadboard_pin_interval)),0)' fill='#888888' x='$(($breadboard_pin_x))' y='$(($breadboard_pin_y))' width='$(($breadboard_pin_width))' height='$(($breadboard_pin_size))'/>"
					breadboard_t_pin_svg+="\n			</g>"

					# breadboard top adapter pins
					breadboard_t_virtual_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$breadboard_adapter_pin_interval)),0)'>"
					breadboard_t_virtual_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					breadboard_t_virtual_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					breadboard_t_virtual_pin_svg+="\n			</g>"

					# schematic top text
					schematic_t_text_svg+="\n				<text x='0' y='$((($length-$i-1)*$schematic_pin_interval))'>${pin_label}</text>"

					# schematic top pins
					schematic_t_pin_svg+="\n				<g transform='translate($((($i%($length/2))*$schematic_pin_interval)),0)'>"
					schematic_t_pin_svg+="\n					<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					schematic_t_pin_svg+="\n					<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					schematic_t_pin_svg+="\n				</g>"

					# pcb top pins
					pcb_t_pin_svg+="\n			<g transform='translate($((($i%($length/2))*$pcb_pin_interval)),0)'>"
					pcb_t_pin_svg+="\n				<use id='connector-${pin_name}-pin' xlink:href='#connector-${template}-pin'/>"
					pcb_t_pin_svg+="\n				<use id='connector-${pin_name}-terminal' xlink:href='#connector-${template}-terminal'/>"
					pcb_t_pin_svg+="\n			</g>"
				fi
			else
				template="${pin_name}"
				# breadboard 1st pin
				breadboard_b_pin_svg+="\n			<g>"
				breadboard_b_pin_svg+="\n				<rect transform='translate(0,0)' fill='#888888' x='$(($breadboard_pin_x))' y='$(($breadboard_pin_y))' width='$(($breadboard_pin_width))' height='$(($breadboard_pin_size))'/>"
				breadboard_b_pin_svg+="\n			</g>"

				# breadboard 1st adpater pin
				breadboard_b_virtual_pin_svg+="\n			<g>"
				breadboard_b_virtual_pin_svg+="\n				<circle id='connector-${pin_name}-pin' cx='$(($breadboard_1st_adapter_pin_x))' cy='$(($breadboard_1st_adapter_pin_y))' r='$(($breadboard_1st_adapter_pin_width))' fill='#FFCC00'/>"
				breadboard_b_virtual_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='$(($breadboard_1st_adapter_pin_x))' cy='$(($breadboard_1st_adapter_pin_y))' r='0.001'/>"
				breadboard_b_virtual_pin_svg+="\n			</g>"

				# schematic bottom text
				schematic_b_text_svg+="\n				<text x='0' y='$(($i*$schematic_pin_interval))'>${pin_label}</text>"

				# schematic 1st pin
				schematic_b_pin_svg+="\n				<g>"
				schematic_b_pin_svg+="\n					<polyline id='connector-${pin_name}-pin' points='$(($schematic_1st_pin_x)),$(($schematic_height-$schematic_pin_size)) $(($schematic_1st_pin_x)),$(($schematic_height))' stroke='#888888'/>"
				schematic_b_pin_svg+="\n					<circle id='connector-${pin_name}-terminal' cx='$(($schematic_1st_pin_x))' cy='$(($schematic_height))' r='0.001' fill='none'/>"
				schematic_b_pin_svg+="\n				</g>"

				# pcb 1st pin
				pcb_b_pin_svg+="\n			<g>"
				pcb_b_pin_svg+="\n				<rect id='connector-${pin_name}-pin' x='$(($pcb_1st_pin_x-$pcb_pin_width/2))' y='$(($pcb_height-$pcb_pin_size))' width='$(($pcb_pin_width))' height='$(($pcb_pin_size))' fill='#FFCC00'/>"
				pcb_b_pin_svg+="\n				<circle id='connector-${pin_name}-terminal' cx='$(($pcb_1st_pin_x))' cy='$(($pcb_height))' r='0.001' fill='none'/>"
				pcb_b_pin_svg+="\n			</g>"
			fi

			i=$(($i+1))
		done

		part_fzp+="${part_connectors_fzp}"

		icon_svg="${default_xml}"
		icon_svg+="\n${default_doc}"
		icon_svg+="\n<svg width='`jq -n $icon_width/100000`in' height='`jq -n $icon_height/100000`in' version='1.1' viewBox='0 0 $(($icon_width)) $(($icon_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		icon_svg+="\n	<g id='icon'>"
		icon_svg+="\n		<g transform='translate(0,$(($icon_pin_size)))'>"
		icon_svg+="\n			<rect width='$(($icon_ic_width))' height='$(($icon_ic_height))' fill='#333333'/>"
		icon_svg+="\n			<text x='$(($icon_ic_name_x))' y='$(($icon_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		icon_svg+="\n			<circle cx='$(($icon_1st_pin_x))' cy='$(($icon_1st_pin_y))' r='$(($icon_1st_pin_radius))'/>"
		icon_svg+="\n		</g>"
		icon_svg+="\n		<g>"
		icon_svg+="\n			<rect id='pin-template' fill='#888888' x='$(($icon_1st_pin_x-$icon_pin_width/2))' y='$(($icon_height-$icon_pin_size))' width='$(($icon_pin_width))' height='$(($icon_pin_size))'/>"
		i="1"; while [ "${i}" -lt "8" ]; do
		icon_svg+="\n			<use x='$(($i*$icon_pin_interval))' xlink:href='#pin-template'/>"
		i=$(($i+1)); done
		icon_svg+="\n		</g>"
		icon_svg+="\n		<g transform='rotate(180 $(($icon_width/2)),$(($icon_height/2)))'>"
		i="0"; while [ "${i}" -lt "8" ]; do
		icon_svg+="\n			<use x='$(($i*$icon_pin_interval))' xlink:href='#pin-template'/>"
		i=$(($i+1)); done
		icon_svg+="\n		</g>"
		icon_svg+="\n	</g>"
		icon_svg+="\n</svg>"

		breadboard_svg="${default_xml}"
		breadboard_svg+="\n${default_doc}"
		breadboard_svg+="\n<svg width='`jq -n $breadboard_width/100000`in' height='`jq -n $breadboard_height/100000`in' version='1.1' viewBox='0 0 $(($breadboard_width)) $(($breadboard_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		breadboard_svg+="\n	<g id='breadboard'>"
		breadboard_svg+="\n		<rect width='$(($breadboard_width))' height='$(($breadboard_height))' fill='#004400'/>"
		breadboard_svg+="\n		<g transform='translate($((($breadboard_width-$breadboard_ic_width)/2)),$((($breadboard_height-$breadboard_ic_height)/2)))'>"
		breadboard_svg+="\n			<rect width='$(($breadboard_ic_width))' height='$(($breadboard_ic_height))' fill='#333333'/>"
		breadboard_svg+="\n			<text x='$(($breadboard_ic_name_x))' y='$(($breadboard_ic_name_y))' fill='#FFFFFF' font-family='OcrA' font-size='${name_font_size}' text-anchor='middle'>${ic_name}</text>"
		breadboard_svg+="\n			<circle cx='$(($breadboard_1st_pin_x))' cy='$(($breadboard_1st_pin_y))' r='$(($breadboard_1st_pin_radius))'/>"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g>"
		breadboard_svg+="${breadboard_b_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g transform='rotate(180 $(($breadboard_width/2)),$(($breadboard_height/2)))'>"
		breadboard_svg+="${breadboard_t_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g>"
		breadboard_svg+="${breadboard_b_virtual_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n		<g transform='rotate(180 $(($breadboard_width/2)),$(($breadboard_height/2)))'>"
		breadboard_svg+="${breadboard_t_virtual_pin_svg}"
		breadboard_svg+="\n		</g>"
		breadboard_svg+="\n	</g>"
		breadboard_svg+="\n</svg>"

		schematic_svg="${default_xml}"
		schematic_svg+="\n${default_doc}"
		schematic_svg+="\n<svg width='`jq -n $schematic_width/100000`in' height='`jq -n $schematic_height/100000`in' version='1.1' viewBox='0 0 $(($schematic_width)) $(($schematic_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		schematic_svg+="\n	<g id='schematic'>"
		schematic_svg+="\n		<g transform='translate(0,$(($schematic_pin_size)))'>"
		schematic_svg+="\n			<rect width='$(($schematic_ic_width))' height='$(($schematic_ic_height))' fill='#FFFFFF' stroke='#000000' stroke-width='$(($multiply))'/>"
		schematic_svg+="\n			<text x='$(($schematic_ic_name_x))' y='$(($schematic_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		schematic_svg+="\n			<circle cx='$(($schematic_1st_pin_x))' cy='$(($schematic_1st_pin_y))' r='$(($schematic_1st_pin_radius))'/>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g transform='rotate(-90)' fill='#888888' font-family='OcrA' font-size='$(($pin_font_size))' text-anchor='middle'>"
		schematic_svg+="\n			<g transform='translate($((-$schematic_height+$schematic_pin_size/2)),$(($schematic_pin_text_offset)))'>"
		schematic_svg+="${schematic_b_text_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n			<g transform='translate($((-$schematic_pin_size/2)),$(($schematic_pin_text_offset)))'>"
		schematic_svg+="${schematic_t_text_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n		<g stroke-width='$(($multiply))'>"
		schematic_svg+="\n			<g>"
		schematic_svg+="${schematic_b_pin_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n			<g transform='rotate(180 $(($schematic_width/2)),$(($schematic_height/2)))'>"
		schematic_svg+="${schematic_t_pin_svg}"
		schematic_svg+="\n			</g>"
		schematic_svg+="\n		</g>"
		schematic_svg+="\n	</g>"
		schematic_svg+="\n</svg>"

		pcb_svg="${default_xml}"
		pcb_svg+="\n${default_doc}"
		pcb_svg+="\n<svg width='`jq -n $pcb_width/100000`in' height='`jq -n $pcb_height/100000`in' version='1.1' viewBox='0 0 $(($pcb_width)) $(($pcb_height))' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>"
		pcb_svg+="\n	<g id='silkscreen'>"
		pcb_svg+="\n		<g transform='translate(0,$(($pcb_pin_size)))'>"
		pcb_svg+="\n			<rect width='$(($pcb_ic_width))' height='$(($pcb_ic_height))' fill='none' stroke='#000000' stroke-width='$(($multiply))'/>"
		pcb_svg+="\n			<circle cx='$(($pcb_1st_pin_x))' cy='$(($pcb_1st_pin_y))' r='$(($pcb_1st_pin_radius))'/>"
		pcb_svg+="\n			<text x='$(($pcb_ic_name_x))' y='$(($pcb_ic_name_y))' font-family='OcrA' font-size='$(($name_font_size))' text-anchor='middle'>${ic_name}</text>"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		pcb_svg+="\n	<g id='copper1'>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+="<g id='copper0'>"
		fi
		pcb_svg+="\n		<g>"
		pcb_svg+="${pcb_b_pin_svg}"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n		<g transform='rotate(180 $(($pcb_width/2)),$(($pcb_height/2)))'>"
		pcb_svg+="${pcb_t_pin_svg}"
		pcb_svg+="\n		</g>"
		pcb_svg+="\n	</g>"
		if [ "${ic_type}" = "SIP" ] || [ "${ic_type}" = "DIP" ]; then
		pcb_svg+='</g>'
		fi
		pcb_svg+="\n</svg>"
	fi

	part_fzp+="\n	</connectors>"
	part_fzp+="\n</module>"

	printf "${part_fzp}" | tr "'" '"' >"${part}"
	printf "${icon_svg}" | tr "'" '"' >"${icon}"
	printf "${breadboard_svg}" | tr "'" '"' >"${breadboard}"
	printf "${schematic_svg}" | tr "'" '"' >"${schematic}"
	printf "${pcb_svg}" | tr "'" '"' >"${pcb}"
	zip -9 -v "${pathname}.fzpz" "${part}" "${icon}" "${breadboard}" "${schematic}" "${pcb}"
	rm "${part}" "${icon}" "${breadboard}" "${schematic}" "${pcb}"
fi